package testapplication;

import java.util.concurrent.TimeUnit;

/*import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;*/
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.WebdriverFactory;

public class TestNGExecution {

	static WebDriver driver;

	@BeforeClass
	public static void setUp() throws Exception {
		System.out.println("Before Step defs");
		if (WebdriverFactory.getDriver() == null) {
			driver = WebdriverFactory.setDriver();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.MILLISECONDS);
			WebdriverFactory.addDriver(driver);
		}
	}

	@Test
	public void testOnChromeWithBrowserStackUrl() {

		driver.get("https://www.browserstack.com/");
		driver.manage().window().maximize();
		System.out.println(
				"this is the test related to chrome browserstack homepage" + " " + Thread.currentThread().getId());

	}

	@Test
	public void testOnChromeWithBrowserStackSignUp() {

		driver.get("https://www.browserstack.com/users/sign_up");
		driver.manage().window().maximize();
		/*
		 * driver.findElement(By.id("user_full_name")).sendKeys("Srinadha");
		 * driver.findElement(By.id("user_email_login")).sendKeys("Srinadha");
		 * driver.findElement(By.id("user_password")).sendKeys("browserstack");
		 * System.out.println( "this is the test related to chrome browserstack login" +
		 * " " + Thread.currentThread().getId());
		 */

	}

	@AfterClass
	public static void close() {
		if (WebdriverFactory.getDriver() != null) {
			WebdriverFactory.closeBrowser();
		}
	}

}
