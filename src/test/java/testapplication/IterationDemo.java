package testapplication;

import java.util.HashMap;
import java.util.Map;

public class IterationDemo {
	public static void main(String[] arg) {
		Map<String, String> map = new HashMap<String, String>();

		// enter name/url pair
		map.put("1", "test");
		map.put("2", "xyz");
		map.put("3", "abc");
		int j = 0;
		// using for-each loop for iteration over Map.entrySet()
		for (Map.Entry<String, String> entry : map.entrySet()) {
			if (j == 2) {
				break;
			}

			System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());

			j++;
		}
	}
}
