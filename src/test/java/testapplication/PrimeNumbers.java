package testapplication;

import java.util.Scanner;

public class PrimeNumbers {
	public static void main(String[] args) {

		int num = 0;
		// Empty String
		String primeNumbers = "";

		Scanner scan = new Scanner(System.in);
		System.out.println("Enter number:");
		int n = scan.nextInt();
		int i = 0;

		for (; i <= n; i++) {
			int counter = 0;
			for (num = i; num >= 1; num--) {
				if (i % num == 0) {
					counter = counter + 1;
				}
			}
			if (counter == 2) {
				// Appended the Prime number to the String
				primeNumbers = primeNumbers + i + " ";
			}
		}
		System.out.println("Prime numbers from 1 to " + n + " are :");
		System.out.println(primeNumbers);

		// Please verify below For loop to understand all three conditions are
		// optional's:
		/**
		 * Below is the for loop syntax::: ********************** for (initialization
		 * statement; condition check; update) loop body;
		 ****************
		 * Initialization: It is the initial condition which is executed once when the
		 * loop starts. Here, we can initialize the variable, or we can use an already
		 * initialized variable. It is an optional condition.
		 *
		 * Condition: It is the second condition which is executed each time to test the
		 * condition of the loop. It continues execution until the condition is false.
		 * It must return boolean value either true or false. It is an optional
		 * condition.
		 *
		 *
		 * Increment/Decrement: It increments or decrements the variable value. It is an
		 * optional condition.
		 *
		 * Statement: The statement of the loop is executed each time until the second
		 * condition is false.
		 *
		 * your for( ; ; ) syntax. It has no initialization statement, so nothing will
		 * be executed. Its conditional check statement is also empty, which means it
		 * evaluates to true after that the loop body is executed. Next, since the
		 * update statement is empty, nothing is executed. Then the conditional check is
		 * performed again which will again evaluates to true and then this whole
		 * process will again repeat.
		 *
		 */

		for (;;) {
			System.out.println(i);

		}
	}
}