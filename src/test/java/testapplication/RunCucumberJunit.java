package testapplication;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = { "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:" }, glue = {
		"testapplication" }, tags = "@testSampleApplication", features = { "src/test/resources/features" })

public class RunCucumberJunit {

}
