package PageElements;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class AllElements {

	@FindBy(css = ".class")
	public WebElement classname;

	@FindBy(id = "foobar")
	public WebElement foobar;

	@FindBy(xpath = "//*[text()='data']")
	public WebElement datafield;

	@FindBy(how = How.TAG_NAME, using = "a")
	public List<WebElement> links;

	@FindBy(how = How.CSS, using = "#contact-link")
	public WebElement contactUs;

	@FindBy(how = How.ID, using = "xyz")
	public WebElement sampleID;

}
