package pageoperations;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import PageElements.AllElements;

public class MainPageOperations {

	protected WebDriver driver;

	AllElements elements = PageFactory.initElements(driver, AllElements.class);

	public MainPageOperations(WebDriver driver) {
		this.driver = driver;
		// elements = PageFactory.initElements(this.driver, AllElements.class);

	}

	public MainPageOperations() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Navigate the given url from feature file
	 *
	 * @param url - Given url to navigate
	 * @throws Exception
	 */
	public void navigateToSpecifiedURL(String url) throws Exception {
		driver.get(url);
		returnOneElement();
	}

	/**
	 * Click on the button using CSS Selector or Xpath passing from feature file
	 *
	 * @param webElement  - Either of CSS Selector or Xpath
	 * @param elementType - If CSS selector is passing from feature file we have to
	 *                    mention 'css' else 'xpath'
	 * @throws Exception
	 */
	public void btnClick(String webElement, String elementType) throws Exception {
		WebElement element = null;
		WebDriverWait wait = new WebDriverWait(driver, 30);
		if (elementType.equalsIgnoreCase("css")) {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(elementType)));
			element = driver.findElement(By.cssSelector(webElement));
		} else if (elementType.equalsIgnoreCase("xpath")) {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementType)));
			element = driver.findElement(By.xpath(webElement));
		}

		element.click();
	}

	/**
	 *
	 * @param value       - data field to enter
	 * @param cssSelector - css selector for given input field
	 * @throws Exception
	 */
	public void input_field_data_entry(String value, String cssSelector) throws Exception {

		/*
		 * Wait<WebDriver> wait = new
		 * FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(30))
		 * .pollingEvery(Duration.ofSeconds(5)).ignoring(NoSuchElementException.class);
		 *
		 * WebElement foo = wait.until(new Function<WebDriver, WebElement>() {
		 *
		 * @Override public WebElement apply(WebDriver driver) { return
		 * driver.findElement(By.cssSelector(cssSelector)); } });
		 */

		WebElement element = driver.findElement(By.cssSelector(cssSelector));
		element.clear();
		element.sendKeys(value);

	}

	public void click_button_ContactUS(String btnName) throws Exception {
		try {
			System.out.println("Button name clicked" + btnName);
			elements.contactUs.click();
		} catch (Exception e) {
			throw e;
		}
	}

	public void clickon(String labelName, String data) throws Exception {
		try {
			WebElement element = driver
					.findElement(By.xpath("//label[text()='" + labelName + "']//following-sibling::input"));
			element.sendKeys(data);

		} catch (Exception e) {
			throw e;
		}
	}

	public void returnOneElement() {
		List<WebElement> elements = driver.findElements(By.cssSelector("#SIvCob a"));

		/*
		 * for (int i = 0; i < elements.size(); i++) {
		 *
		 * }
		 */
		int row = 0;
		for (WebElement xyz : elements) {
			System.out.println("Language names::" + xyz.getText());
			if (row == 2) {
				break;
			}
			row++;
		}

	}

}
