package utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.json.JSONObject;

public class MainClass {

	private static final String CHROME_DRIVER_WIN = BasePage.getProperty("chromedriverpath");

	/*
	 * public static void main(String args[]) throws IOException {
	 *
	 * URL url = new URL(
	 * "https://icms-dev.taqadhi.site/prweb/api/FIRegisterCase/v1/FIRegisterCase");
	 *
	 * HttpURLConnection con = (HttpURLConnection) url.openConnection();
	 *
	 * con.setRequestMethod("POST");
	 *
	 * con.setRequestProperty("Content-Type", "application/json; utf-8");
	 *
	 * con.setRequestProperty("Accept", "application/json");
	 *
	 * con.setDoOutput(true);
	 *
	 * File file = new File("src/test/resources/Payload.json");
	 *
	 * BufferedReader br = new BufferedReader(new FileReader(file)); br.close();
	 *
	 * String st; while ((st = br.readLine()) != null) { System.out.println(st); }
	 *
	 * }
	 */

	public static void main(String[] args) throws Exception {

		// testapipostcode("CF117FF");

		Map<Integer, String> map = new HashMap<Integer, String>();
		// Adding elements to map
		map.put(1, "Amit");
		map.put(5, "Rahul");
		map.put(2, "Jai");
		map.put(6, "Amit");

		// **************************ComparingByKey****************

		// Returns a Set view of the mappings contained in this map
		map.entrySet()
				// Returns a sequential Stream with this collection as its source
				.stream()
				// Sorted according to the provided Comparator
				.sorted(Map.Entry.comparingByKey())
				// Performs an action for each element of this stream
				.forEach(System.out::println);

		// **************For Each*********************

		for (Entry<Integer, String> entry : map.entrySet()) {

			System.out.println("Key::: " + entry.getKey() + " Value:::" + entry.getValue());

		}

		// ************************Iterator*************************
		// Traversing Map

		Set<Map.Entry<Integer, String>> set = map.entrySet();// Converting to Set so that we can traverse

		Iterator<Map.Entry<Integer, String>> itr = set.iterator();
		while (itr.hasNext()) { // Converting to Map.Entry so that we can get key and value separately

			Map.Entry entry = itr.next();
			System.out.println(entry.getKey() + " " + entry.getValue());
		}

		// *******************************************************************

		/*
		 * System.setProperty("webdriver.chrome.driver", CHROME_DRIVER_WIN); WebDriver
		 * driver = new ChromeDriver();
		 *
		 * driver.get("");
		 *
		 * WebDriver driverff = new FirefoxDriver(); driverff.get("");
		 *
		 * driver.get("https://www.tutorialspoint.com/index.htm");
		 *
		 * KeysForSelenium test = KeysForSelenium.ADD; System.out.println(test);
		 *
		 * // identify element WebElement m =
		 * driver.findElement(By.xpath("//*[local-name()='svg' and @data-icon='home']"))
		 * ; // Action class to move and click element Actions act = new
		 * Actions(driver); // act.moveToElement(m).click().build().perform();
		 * driver.close();
		 * Runtime.getRuntime().exec("taskkill /F /IM chromedriver.exe /T");
		 */

		// ********************************Dictory Methods************************

		// Initializing a Dictionary
		Dictionary<String, String> geek = new Hashtable<String, String>();

		// put() method
		geek.put("123", "Coding");
		geek.put("456", "Program");

		// geek.put("567", null);

		// elements() method :

		for (Enumeration<String> i = geek.elements(); i.hasMoreElements();) {
			System.out.println("Value in Dictionary : " + i.nextElement());
		}

		// get() method :
		System.out.println("\nValue at key = 6 : " + geek.get("6"));
		System.out.println("Value at key = 123 : " + geek.get("123"));

		// isEmpty() method :
		System.out.println("\nThere is no key-value pair : " + geek.isEmpty() + "\n");

		// keys() method :
		for (Enumeration<String> k = geek.keys(); k.hasMoreElements();) {
			System.out.println("Keys in Dictionary : " + k.nextElement());
		}

		String firstime = geek.remove("123");
		// remove() method :
		System.out.println("\nRemove null length: " + firstime);
		System.out.println("Check the value of removed key : " + geek.get("123").length());
		try {
			String value = geek.remove(geek.get("123").length());
			System.out.println("\nNull pointer exception : " + value);
		} catch (Exception e) {
			throw e;
		}

		System.out.println("\nSize of Dictionary : " + geek.size());

	}

	public static boolean test(String firstString, String secondString) {

		if (firstString.contains(secondString)) {
			return true;
		} else {
			return false;
		}

	}

	public static void readCSVData() {

		String line = "";
		try {
			// parsing a CSV file into BufferedReader
			BufferedReader br = new BufferedReader(new FileReader("CSVDemo.csv"));
			while ((line = br.readLine()) != null) {
				String[] usersCredentials = line.split(","); // use comma as separator
				System.out.println("Username:" + usersCredentials[0] + ", Password:" + usersCredentials[1]);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void testapipostcode(String postcode) {
		try {
			// URL url = new URL("http://api.postcodes.io/postcodes/" + postcode);

			// Object test = url;

			HttpClient client = HttpClient.newHttpClient();

			HttpRequest request = HttpRequest.newBuilder().uri(new URI("http://api.postcodes.io/postcodes/" + postcode))
					.GET().build();

			/*
			 * client.sendAsync(request,
			 * BodyHandlers.ofString()).thenApply(HttpResponse::body)
			 * .thenAccept(System.out::println).join();
			 */

			HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
			// System.out.println(response.statusCode());
			// System.out.println(response.body());

			JSONObject obj = new JSONObject(response.body());

			Map<String, Object> map = new HashMap<String, Object>();
			Iterator<String> keys = obj.keys();
			while (keys.hasNext()) {
				String key = keys.next();
				Object value = obj.get(key);

				map.put(key, value);
			}

			// System.out.println(test.toString());

			/*
			 * HttpURLConnection http = (HttpURLConnection) url.openConnection();
			 * System.out.println(http.getResponseCode() + " " + http.getResponseMessage());
			 * http.disconnect();
			 */

			/*
			 * String s = "hello"; Object obj = s; System.out.println(obj.getClass());
			 */
			// System.out.println(test.getClass());

		} catch (Exception ex) {

		}
	}

}
