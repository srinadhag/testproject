package utils;

class Student implements Comparable<Student> {
	private String student;

	public String getStudent() {
		return student;
	}

	public void setStudent(String student) {
		this.student = student;
	}

	public int getSno() {
		return sno;
	}

	public void setSno(int sno) {
		this.sno = sno;
	}

	private int sno;

	public Student(String student, int sno) {
		this.student = student;
		this.sno = sno;
	}

	@Override
	public int compareTo(Student s) {
		return student.compareTo(s.getStudent());
	}

	@Override
	public String toString() {
		return student + "-" + sno;
	}

}
